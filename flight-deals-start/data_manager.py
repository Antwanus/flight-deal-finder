import requests as requests

SHEETY_API_URL = 'https://api.sheety.co/47f9c4ec8268d433ed39542adf98442e/flightdeals/prices'
sheety_headers = {
    'Authorization': 'Bearer lkjdsqfhklqjdshflujdk'
}


def update_lowest_price(id, new_price):
    new_data = {
        'price': {
            'lowestPrice': new_price
        }
    }
    put_response = requests.put(url=f"{SHEETY_API_URL}/{id}", json=new_data)


class DataManager:
    # This class is responsible for talking to the Google Sheet.
    def __init__(self):
        self.prices_list = {}

    def get_all(self) -> [{}]:
        response_sheety = requests.get(url=SHEETY_API_URL, headers=sheety_headers)
        response_sheety.raise_for_status()
        self.prices_list = response_sheety.json()['prices']
        return self.prices_list

    def add_row(city: str, iata: str, lowest_price: float):
        post_req_body = f'{{"price": {{"city": {city}, "iataCode": {iata}, "lowestPrice": {lowest_price}}}}}'
        print(post_req_body)
        post_response = requests.post(url=SHEETY_API_URL, json=post_req_body, headers=sheety_headers)
        post_response.raise_for_status()
        print(post_response.text)

    def update_iata_code(self):
        for row in self.prices_list:
            print(row)
            new_data = {
                'price': {
                    'iataCode': row['iataCode']
                }
            }
            put_response = requests.put(url=f"{SHEETY_API_URL}/{row['id']}", json=new_data)


