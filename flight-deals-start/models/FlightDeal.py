class FlightDeal:
    def __init__(self, city: str, iata: str, lowest_price: float):
        self.city = city
        self.iata = iata
        self.lowest_price = lowest_price
