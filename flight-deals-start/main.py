from datetime import datetime, timedelta
from data_manager import DataManager, update_lowest_price
from flight_search import *
from pprint import pprint

data_manager = DataManager()
sheet_data = data_manager.get_all()
# sheet_data = [
#     {'city': 'Paris', 'iataCode': 'PAR', 'id': 2, 'lowestPrice': 54},
#     {'city': 'Berlin', 'iataCode': 'BER', 'id': 3, 'lowestPrice': 100},
#     {'city': 'Tokyo', 'iataCode': 'TYO', 'id': 4, 'lowestPrice': 4850},
#     {'city': 'Sydney', 'iataCode': 'SYD', 'id': 5, 'lowestPrice': 5510},
#     {'city': 'Istanbul', 'iataCode': 'IST', 'id': 6, 'lowestPrice': 950},
#     {'city': 'Kuala Lumpur', 'iataCode': 'KUL', 'id': 7, 'lowestPrice': 4140},
#     {'city': 'New York', 'iataCode': 'NYC', 'id': 8, 'lowestPrice': 2400},
#     {'city': 'San Francisco', 'iataCode': 'SFO', 'id': 9, 'lowestPrice': 2600},
#     {'city': 'Cape Town', 'iataCode': 'CPT', 'id': 10, 'lowestPrice': 3780}
# ]
ORIGIN_CITY = 'BRU'

# CHECK IF XLS NEEDS TO BE UPDATED WITH IATA_CODE
for row in sheet_data:
    if row['iataCode'] == '':
        print('There was no iataCode found for', row['city'])

        row['iataCode'] = get_destination_code(row['city'])
        print('found: ', row['iataCode'])

        data_manager.prices_list = sheet_data
        data_manager.update_iata_code()
    else:
        print('IATA code is present in the file. Proceeding...')

tomorrow = datetime.now() + timedelta(days=1)
today_plus_6_months = datetime.now() + timedelta(days=(6 * 30))
today_plus_1_year = datetime.now() + timedelta(days=364)

# CHECK FOR FLIGHTS FOR EACH ENTRY IN XLS
for destination in sheet_data:
    flight: FlightData = check_flights(origin_city_code=ORIGIN_CITY,
                                       destination_city_code=destination['iataCode'],
                                       from_time=tomorrow,
                                       to_time=today_plus_1_year)

    if flight and flight.price < destination['lowestPrice']:
        update_lowest_price(destination['id'], flight.price)
        destination['lowestPrice'] = flight.price
